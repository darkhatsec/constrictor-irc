#!/usr/bin/python           
 #Constrictor IRC Copyright (C) Will Fleury 
 #This program is free software: you can redistribute it and/or modify
 #it under the terms of the GNU General Public License as published by
 #the Free Software Foundation, either version 3 of the License, or
 #(at your option) any later version.
 #
 #This program is distributed in the hope that it will be useful,
 #but WITHOUT ANY WARRANTY; without even the implied warranty of
 #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #GNU General Public License for more details.

 #You should have received a copy of the GNU General Public License
 #along with this program.  If not, see <http://www.gnu.org/licenses/>



# Command application in active development for accessing IRC networks through TOR. 

# Written by SGT_Vendeada/Will Fleury

# NOTE: You must have th TOR TCP control port option enabled (Vidalia -> Settings -> Advanced. Then Click "Use TCP Connection" the address should be your local loopback interface (127.0.0.1) and the port you set it to cannot be bound to another service running. The client is dependant upon TOR connection and will fail with a connection refused error if TOR isn't running. 

# NOTE: The application may display false positives if there is another network service on the port specified, so verify by clicking "Message Log" on the vidalia main menu, click the advanced tab. Then run the app, type the port, and check the log. if it says "Notice: New Control Connection Opened" then you are doing everything correctly. Otherwise use the above steps and adjust the port number.

#Not all features are functional yet!

from Tkinter import *
import threading
import socket 
print "                               "
print "ConstricTOR-IRC Version 0.1"
print "                               "
print "Author: Will Fleury"
print " "
print "Version: 0.1.1"
print " "
print "Brining the flexibility of Python, and Anonymity of TOR together "
print "for your IRC Convienience!"
print " "
s = socket.socket()         # Create a socket object
thost = socket.gethostname() # Get local machine name
tport = int(input("Please enter your TOR clients control port (Example: 9050, or 9051): ")) # TOR service port input
s.connect((thost, tport)) #Socket connection to your local tor client (piggy back)
print s.recv #Recieve data from tor to confirm
print "Connected to TOR!" #Display when connected to TOR




host = str(raw_input("IRC Server: "))
port = int(input("Port: "))
chan = str(raw_input("Channel #: "))
ident = str(raw_input("ident: "))
real = str(raw_input("'real' name:  "))
nick = str(raw_input("nick: "))

irc = socket.socket()
irc.settimeout(300)
connected = False

def connection(host, port, nick, ident, real, chan):
    global connected
    try:
        irc.connect((host,port))
        irc.send("NICK {}\r\n".format(nick))
        irc.send("USER {} {} bla :{}\r\n".format(ident, host, real))
        connected = True

    except socket.error:
        print "ConstricTOR is attempting to connect you to the specified server and channel..."
        #time.sleep(5)

def parseout(out):
    cmdpart = out[1:]
    msgpart = out[1:]
    global chan
    global irc
    global nick

    if cmdpart == '/':
        cmd = msgpart.split(' ')
        if cmd[0] == 'quit':
            m = cmd[1:]
            s = ''
            for x in m:
                s += x + ' '
                
            irc.send('QUIT :{}\r\n'.format(s))
        if cmd[0] == 'join':
            irc.send('JOIN :{}\r\n'.format(cmd[1]))
            irc.send('PART :{}\r\n'.format(chan))
            chan = cmd[1]
        if cmd[0] == 'part':
            irc.send('PART :{}\r\n'.format(cmd[1]))
            chan = ''
        if cmd[0] == 'nick':
            irc.send('NICK '+cmd[1]+'\r\n')
            nick = cmd[1]
        if cmd[0] == 'me':
            m = cmd[1:]
            s = ''

            for x in m:
                s += x + ' '
                
            irc.send('PRIVMSG '+chan+' :\001ACTION {}\001\r\n'.format(s))
        if cmd[0] == 'msg':
            s = ''
            m = cmd[2:]

            for x in m:
                s += x + ' '
            irc.send('PRIVMSG '+cmd[1]+' :{}\r\n'.format(s))
        if cmd[0] == 'spam':
            for x in range(0, int(cmd[1])):
                irc.send("PRIVMSG "+chan+" :SPAM!\r\n")
            
    else:
        irc.send('PRIVMSG {} :{}\r\n'.format(chan, out))

class App:
    def __init__(self, master):
        frame = Frame(master)
        frame.pack()

        self.createWidgets(frame)

    def createWidgets(self, master):
        self.out = Text(master)
        #self.out['state'] = DISABLED
        self.out.pack(fill=Y)

        self.scrollbar = Scrollbar(master)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        
        self.e = StringVar()
        self.entry = Entry(master)
        self.entry['textvariable'] = self.e
        self.entry.pack(side='left', fill=X)
        self.entry.bind('<Return>', self.send)

        self.scrollbar.config(command=self.out.yview)
        self.out.config(yscrollcommand=self.scrollbar.set)

        self.button = Button(master)
        self.button['text'] = 'Send'
        self.button['fg'] = 'red'
        self.button['width']= 12
        self.button['command'] = self.send
        self.button.pack(side='right', fill=X)

    def send(self, event=None):
        parseout(self.e.get())
        self.out.insert(END, '{}: {}\n'.format(nick, self.e.get()))
        self.e.set('')

    def GetData(self):
        global irc
        global connected
        while connected:
            try:
                data = irc.recv(4096)

                if len(data) == 0:
                    break

                if data[0:4] == 'PING':
                    irc.send('PONG '+data.split()[1]+'\r\n')
                    irc.send('JOIN '+chan+'\r\n')
                    irc.send('PRIVMSG ChanServ :identify gamerkid\r\n')
                
                if data.find('PRIVMSG') != -1:
                    complete=data[1:].split(':',1)
                    info=complete[0].split(' ') 
                    msgpart=complete[1] 
                    sender=info[0].split('!')

                    self.out.insert(END, '{}: {}'.format(sender[0], msgpart))
                
                if data.find('Nickname is already in use') != -1:
                    self.out.insert(END, 'Nickname is already in use.')

                if data.find('Password accepted') != -1:
                    self.out.insert(END, 'Password accepted, you are now recognized.\n')
                    
            except socket.timeout:
                connected = False
                print connected
                connection(host, port, nick, ident, real, chan)

connection(host, port, nick, ident, real, chan)
root = Tk()

app = App(root)
thread = threading.Thread(target=app.GetData)
thread.start()

root.mainloop()
thread.join()
